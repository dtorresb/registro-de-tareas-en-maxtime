package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.pageRegistro;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Completar;
import co.com.proyectobase.screenplay.tasks.ingresar;
import co.com.proyectobase.screenplay.tasks.registrar;
import co.com.proyectobase.screenplay.userinterface.registroPage;
import co.com.proyectobase.screenplay.util.ExcelReader;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.findby.By;

public class maxTimeDefinitionSteps {
	@Managed(driver = "Chrome")
	private WebDriver hisbrowser;
	private Actor Daniel = Actor.named("Daniel");
	

	@Before
	public void configuracionInicial() {
		Daniel.can(BrowseTheWeb.with(hisbrowser));
	}
	
	
	@Given("^Daniel wants to start session in the application$")
	public void danielQuiereIniciarSesionEnElAplicativo() throws Exception {
		Daniel.wasAbleTo(Abrir.PaginaDeInicio());
		Daniel.wasAbleTo(Completar.formularioInicioDeSesion());
		Daniel.wasAbleTo(ingresar.alDia());
	}

	@When("^He performs the registration of activities$")
	public void elRealizaElRegistroDeLasActividades(DataTable dataform) throws Exception {
		WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
		List<List<String>> data = dataform.raw();
		
		if (driver.findElement(By.xpath(
				"//table[contains(@id,'_dviLaboral_View')]/tbody/tr/td//span/input[contains(@id,'xaf_dviLaboral_View_S')]"))
				.getAttribute("value").equals("C")) {
			for (int i = 0; i < data.size(); i++) {
				Daniel.wasAbleTo(registrar.misActividades(data, i));
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO: handle exception
				}
			}	
		}
		
		
		
		
	}

	@Then("^The day must be closed$")
	public void elDiaSeDebeCerrar() throws Exception {
		Daniel.should(GivenWhenThen.seeThat(pageRegistro.compararHorasLaboradas()));
		//Daniel.should(GivenWhenThen.seeThat(registroPage.diasReportados, Matchers.equalTo("9")));
	}
	
}
