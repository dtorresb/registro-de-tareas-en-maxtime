#Author: dtorresb@choucairtesting.com
#language:en
@tag
Feature: record of activities in MaxTime
  As a tester
  I want to make the activity report in MaxTime
  To fulfill the task that I must carry out daily

  @tag1
  Scenario: record of activities
    Given Daniel wants to start session in the application

    When He performs the registration of activities
    
        ##@externaldata@./src/test/resources/datadriven/dtDatos.xlsx@Datos
   |1   | H. Choucair     | cbo         |CAPACITACIÓN BÁSICA OPERATIVA Y/O ESPECÍFICA   |5   | Automatización de registro de actividades y tiempos en Maxtime|
   |2   | H. Choucair     | cbo         |CAPACITACIÓN BÁSICA OPERATIVA Y/O ESPECÍFICA   |4   |ejemplo 2|

    Then The day must be closed
    
    
