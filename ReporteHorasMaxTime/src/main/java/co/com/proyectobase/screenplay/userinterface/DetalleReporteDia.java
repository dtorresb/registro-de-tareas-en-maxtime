package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class DetalleReporteDia extends PageObject{
	public static final Target proyecto = Target.the("seleccion del proyecto").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l128_xaf_dviProyecto_Edit_Find_BImg\']"));
	public static final Target proyectoda = Target.the("seleccion del proyecto").located(By.xpath("//*[@id=\"PopupWindowCallbackPanel_PopupWindow33363460_PW-1\"]"));
	
	public static final Target seleccion_proyecto = Target.the("seleccion del proyecto en ventana emergente, siempre el primero que se presenta").located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[2]/td[4]/table/tbody/tr/td/span"));
	
	public static final Target tipo_hora = Target.the("seleccion del tipo de hora").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_I\']"));
	public static final Target tipo_hora2 = Target.the("seleccion del tipo de hora en el campo desplegado").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l148_xaf_dviTipoHora_Edit_DD_DDD_L_LBT\']"));
	
	public static final Target boton_servicio= Target.the("seleccion del boton servicio").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l153_xaf_dviServicio_Edit_Find_BImg\']"));
	public static final Target caja_buscar_servicio= Target.the("Caja de texto para buscar servicio").located(By.xpath("//*[@id=\'Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed_I\']"));
	public static final Target boton_buscar_servicio= Target.the("boton para buscar servicio").located(By.xpath("//*[@id=\'Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD\']/span"));
	public static final Target seleccion_servicio= Target.the("seleccionar el servicio").located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[2]/td[3]/table/tbody/tr/td/span"));
	public static final Target seleccion_actividad= Target.the("seleccionar la actividad").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD\']/tbody/tr/td[2]"));
	public static final Target seleccion_actividad2= Target.the("seleccionar la actividad").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l158_xaf_dviActividad_Edit_DD_DDD_L_LBT\']"));
	
	public static final Target horas_ejecutadas= Target.the("horas ejecutadas").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l182_xaf_dviHoras_Edit_I\']"));
	public static final Target comentario= Target.the("comentario").located(By.xpath("//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l207_xaf_dviComentario_Edit_I\']"));
	public static final Target boton_guardar= Target.the("Boton guardar").located(By.xpath("//*[@id=\'Vertical_EditModeActions2_Menu_DXI0_T\']/a"));
	public static final Target boton_guardar_cerrar= Target.the("Boton guardar y cerrar").located(By.xpath("//*[@id=\'Vertical_EditModeActions2_Menu_DXI1_T\']/a"));
									 
	
	public static final Target boton_guardar_nuevo= Target.the("Boton guardar y nuevo").located(By.xpath("//*[@id=\'Vertical_EditModeActions2_Menu_DXI2_T\']/a"));
	
	
	
	
}
