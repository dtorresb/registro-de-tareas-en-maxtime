package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.userinterface.mainPageMaxTime;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class ingresar implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO: handle exception
		}*/
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(mainPageMaxTime.SeleccionDia));
		
	}

	public static ingresar alDia() {
		return Tasks.instrumented(ingresar.class);
	}

}
