package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.InicioSesionMaxTime;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	private InicioSesionMaxTime inicioMaxTime;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(inicioMaxTime));
				
	}

	public static Abrir PaginaDeInicio() {
		return Tasks.instrumented(Abrir.class);
	}

}
