package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class mainPageMaxTime extends PageObject{
	public static final Target SeleccionDia = Target.the("seleccion del primer dia").located(By.xpath("//table[contains(@class,'dxgvTable_Office2010Blue')]/tbody/tr[2]/td[3]/table/tbody/tr/td/span"));
	
}
