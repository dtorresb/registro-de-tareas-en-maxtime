package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.InicioSesionMaxTime;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;


public class Completar implements Task{
	
	private String Usuario="dtorresb";
	private String Contrasena="by2w3bvy";
	private InicioSesionMaxTime maxTimeInicio;


	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(Usuario).into(maxTimeInicio.usuario));
		actor.attemptsTo(Enter.theValue(Contrasena).into(maxTimeInicio.contrasena));
		actor.attemptsTo(Click.on(maxTimeInicio.conectarse));
	}
	
	public static Completar formularioInicioDeSesion() {
		return Tasks.instrumented(Completar.class);
	}

}
