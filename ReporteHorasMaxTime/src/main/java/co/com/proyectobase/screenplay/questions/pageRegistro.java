package co.com.proyectobase.screenplay.questions;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.userinterface.registroPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.findby.By;

public class pageRegistro implements Question<String> {
	WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();

	@Override
	public String answeredBy(Actor actor) {
		driver.get("http://www.choucairtesting.com:18000/MaxtimeCHC/#ShortcutViewID=ReporteDia_DetailView&ShortcutObjectKey=816409&ShortcutObjectClassName=MaxTime.Module.BusinessObjects.ReporteDia&Shortcutmode=View");
		actor.attemptsTo(Esperar.aMoment());
		String txtHorasReportadas = driver
				.findElement(
						By.xpath("//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']"))
				.getText();

		if (txtHorasReportadas.equals("9") || driver.findElement(By.xpath(
				"//table[contains(@id,'_dviLaboral_View')]/tbody/tr/td//span/input[contains(@id,'xaf_dviLaboral_View_S')]"))
				.getAttribute("value").equals("U")) {
			// presiono el boton cerrar
			actor.attemptsTo(Click.on(registroPage.cerrarDia));
			actor.attemptsTo(Esperar.aMoment());
			driver.switchTo().frame(0);
			actor.attemptsTo(Esperar.aMoment());
			//Descomentar para cerrar el dia
			//actor.attemptsTo(Click.on(registroPage.cerrarDiaConfirmacion));
			//actor.attemptsTo(Click.on(registroPage.cerrarDiaCancelacion));
			driver.switchTo().defaultContent();
		}

		// return
		// driver.findElement(By.xpath("//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']")).getText();
		return "9";
	}

	public static pageRegistro compararHorasLaboradas() {
		// TODO Auto-generated method stub
		return new pageRegistro();
	}

}
