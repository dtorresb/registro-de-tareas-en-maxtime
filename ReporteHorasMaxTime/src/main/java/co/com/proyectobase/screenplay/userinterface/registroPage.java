package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class registroPage extends PageObject{
	public static final Target nuevaActividad = Target.the("seleccion del primer dia").located(By.xpath("//*[@id=\'Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_ToolBar_Menu_DXI0_T\']/a"));
	public static final Target diasReportados = Target.the("Dias reportados").located(By.xpath("//*[@id=\'Vertical_v3_MainLayoutView_xaf_l98_xaf_dviTotalHorasReportadas_View\']"));
	public static final Target cerrarDia = Target.the("Cerrar dia").located(By.xpath("//*[@id=\"Vertical_TB_Menu_DXI1_T\"]/a"));
	public static final Target cerrarDiaConfirmacion = Target.the("Cerrar dia").located(By.xpath("//*[@id=\'Dialog_actionContainerHolder_Menu_DXI0_T\']/a"));
	public static final Target cerrarDiaCancelacion = Target.the("Cerrar dia").located(By.xpath("//*[@id=\'Dialog_actionContainerHolder_Menu_DXI1_T\']/a"));
	
	
	
	
	
}
