package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.userinterface.DetalleReporteDia;
import co.com.proyectobase.screenplay.userinterface.registroPage;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import org.openqa.selenium.WebDriver;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.findby.By;

public class registrar implements Task {

	WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();

	private List<List<String>> data;
	private int id;
	private int data_size;

	public registrar(List<List<String>> data, int id) {
		super();
		this.data = data;
		this.id = id;
		this.data_size = data.size();
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
			if (id == 0) {
				actor.attemptsTo(Click.on(registroPage.nuevaActividad));
				actor.attemptsTo(Esperar.aMoment());
			}
			actor.attemptsTo(Click.on(DetalleReporteDia.proyecto));
			actor.attemptsTo(Esperar.aMoment());
			driver.switchTo().frame(0);

			actor.attemptsTo(Click.on(DetalleReporteDia.seleccion_proyecto)); // Siempre se selecciona el primero que se
																				// presenta

			driver.switchTo().defaultContent();
			// try {Thread.sleep(2000);} catch (InterruptedException e) {}
			actor.attemptsTo(Esperar.aMoment());
			actor.attemptsTo(Click.on(DetalleReporteDia.tipo_hora));
			actor.attemptsTo(SeleccionarLista.Desde(DetalleReporteDia.tipo_hora2, data.get(id).get(1).trim()));
			actor.attemptsTo(Esperar.aMoment());

			actor.attemptsTo(Click.on(DetalleReporteDia.boton_servicio));
			actor.attemptsTo(Esperar.aMoment());
			driver.switchTo().frame(0);

			actor.attemptsTo(Enter.theValue(data.get(id).get(2).trim()).into(DetalleReporteDia.caja_buscar_servicio));
			actor.attemptsTo(Click.on(DetalleReporteDia.boton_buscar_servicio));
			// try {Thread.sleep(2000);} catch (InterruptedException e) {}
			actor.attemptsTo(Esperar.aMoment());
			actor.attemptsTo(Click.on(DetalleReporteDia.seleccion_servicio));
			driver.switchTo().defaultContent();
			actor.attemptsTo(Esperar.aMoment());

			actor.attemptsTo(Click.on(DetalleReporteDia.seleccion_actividad));
			actor.attemptsTo(
					SeleccionarLista.Desde(DetalleReporteDia.seleccion_actividad2, data.get(id).get(3).trim()));
			actor.attemptsTo(Enter.theValue(data.get(id).get(4).trim()).into(DetalleReporteDia.horas_ejecutadas));
			actor.attemptsTo(Enter.theValue(data.get(id).get(5).trim()).into(DetalleReporteDia.comentario));

			if (id == data_size-1) { // Ya se llego al final, se debe de cerrar
				
				actor.attemptsTo(Click.on(DetalleReporteDia.boton_guardar_cerrar));
				actor.attemptsTo(Esperar.aMoment());
			} else {
				actor.attemptsTo(Click.on(DetalleReporteDia.boton_guardar_nuevo));
				actor.attemptsTo(Esperar.aMoment());
			}

			actor.attemptsTo(Esperar.aMoment());

		} /*else {
			if (id == data_size - 1) {
				actor.attemptsTo(Click.on(registroPage.cerrarDia));
			}
		}*/
	

	public static registrar misActividades(List<List<String>> data, int id) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(registrar.class, data, id);
	}

}
