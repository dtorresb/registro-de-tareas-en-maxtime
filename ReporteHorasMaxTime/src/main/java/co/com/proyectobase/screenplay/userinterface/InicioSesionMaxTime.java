package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxTimeCHC/Login.aspx")
public class InicioSesionMaxTime extends PageObject{
	public static final Target usuario = Target.the("Usuario").located(By.xpath("//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I\']"));
	public static final Target contrasena = Target.the("Contrasena").located(By.xpath("//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I\']"));
	public static final Target conectarse = Target.the("Contrasena").located(By.xpath("//*[@id=\'Logon_PopupActions_Menu_DXI0_T\']/a"));
	
}
